module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "import/no-extraneous-dependencies": 0,
    "import/no-unresolved": 0,
    "import/no-dynamic-require": 0,
    "no-console": 0,
    "no-unused-vars": 1,
    "strict": 0,
    "no-param-reassign": ["error", { "props": false }]
  }
};