const GitHub = require('github-api');
const config = require('config');
 
module.exports = new GitHub(config.github);