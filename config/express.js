const express = require('express');
const config = require('config');
const { MAX_SIZE } = require('config/constants')
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload')
const allowCors = require('../app/lib/middleware/allow-cors')
const health = require('../app/lib/middleware/health')

const passport = require('config/passport');

// const index = require('../app/routes/index');
const auth = require('../app/routes/api/auth.routes');
const api = require('app/routes/api');
const app = express();

//
app.use(passport.initialize());

// view engine setup
app.set('views', path.join('app/views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
if (config.isDevelopment) {
  app.use(logger('dev'));
}

app.use(fileUpload());

app.use(bodyParser.json({ limit: MAX_SIZE }));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join('public')));
app.use('/apidoc', express.static('apidoc'))

require('app/lib/middleware/helmet')(app);

// Enable CORS on the express server
app.use(allowCors);

app.use('/auth', auth);
app.use('/api/v1', api);
if (config.isTest) {
  app.use('/test', require('app/routes/test/test'));
}

app.use('/', health);

// error handler
require('app/lib/middleware/errror-handler')(app);

module.exports = app;
