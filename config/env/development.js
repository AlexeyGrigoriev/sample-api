'use strict';

module.exports = {
  mongo: process.env.MONGO_URL || 'mongodb://localhost:27017/sample-api-dev',
  hostname: process.env.HOST_NAME || 'localhost',
};
