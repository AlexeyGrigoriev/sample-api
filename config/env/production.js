/**
 * Created by alexey on 14.06.17.
 */

"use strict";

module.exports = {
  mongo: process.env.MONGO_URL || 'mongodb://localhost:27017/sample-api-prod',
  hostname: process.env.HOST_NAME || 'localhost',
};