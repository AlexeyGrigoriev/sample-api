/**
 * Created by alexey on 14.06.17.
 */

'use strict';

module.exports = {
  isTest: true,
  mongo: process.env.MONGO_URL || 'mongodb://localhost:27017/sample-api-test',
  baseUrl: process.env.BASE_URL || 'http://localhost:6030',
};