/**
 * Created by alexey on 14.06.17.
 */

'use strict';

const _ = require('lodash');
const Bb = require('bluebird');
const log = require('config/log')(module);
const config = require('config');
const mongoose = require('mongoose');
const fs = require('fs');
const modelsHomePath = 'app/models';

// mongoose.set('debug', true);

mongoose.Promise = Bb;

mongoose.connect(config.mongo, (err) => {
  if (err) {
    log.error('Cannot connect to mongo');
    return console.log(err);
  }
  log.info(`Connected to mongodb: ${config.mongo}`);
});

const modelFileNames = fs.readdirSync(modelsHomePath);

_.each(modelFileNames, fileName => {
  require(`${modelsHomePath}/${fileName}`)(mongoose);
});


module.exports = mongoose;