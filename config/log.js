/**
 * Created by alexey on 14.06.17.
 */

'use strict';

const winston = require('winston');
const Console = winston.transports.Console;
const path = require('path');

module.exports = function (module) {
  const moduleName = path.basename(module.filename);
  return winston.createLogger({
    transports: [
      new Console({
        colorize: true,
        label: moduleName,
        level: 'silly'
      })
    ]
  })
};