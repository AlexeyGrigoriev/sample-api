/**
 * Created by alex on 5/12/17.
 */


'use strict';

const Bb = require('bluebird');
const nodemailer = require('nodemailer');
const config = require('config');
const options = config.isTest ? {
  jsonTransport:  true,
} : config.email.options;

module.exports = {
  transport: Bb.promisifyAll(nodemailer.createTransport(options)),
  mailOptions: { from: config.email.from },
};
