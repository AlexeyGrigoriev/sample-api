/**
 * Created by alex on 10/9/17
 */

"use strict";

module.exports = {
  'WRONG_LOGIN': 'Login or password is wrong',
  'ALREADY_EXISTS': 'User with such email already exists',
  'CANNOT_FIND': 'The item can not be found',
  'MY_PLACEHOLDER_NOT_ALLOWED': 'Only authenticated users can use `my` placeholder',
  'EMAIL_PASS_REQUIRED': 'Email and password should be specified',
}