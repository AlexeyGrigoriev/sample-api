/**
 * Created by alexey on 14.06.17.
 */

'use strict';

const { env } = process;
const envType = env.NODE_ENV;
const extended = require(`config/env/${envType}`);

const config = {
  appName: 'SampleApi',
  port: env.PORT || 6030,
  host: env.HOST || 'localhost',
  hostName: env.HOST,
  secret: env.SERVER_SECRET,
  github: {
    username: env.GH_USERNAME,
    password: env.GH_PASSWORD
  },
  apixu: {
    key: env.APIXU_KEY
  },
  email: {
    from: 'SampleApi <no-reply@gmail.com>',
    options: {
      service: env.MAILER_SERVICE_PROVIDER || 'gmail',
      auth: {
        user: env.EMAIL_ADDR,
        pass: env.EMAIL_PASS,
      },
    },
  },
  isDev: env.NODE_ENV === 'development',
  isTest: env.NODE_ENV === 'test',
  isProd: env.NODE_ENV === 'production',
};


Object.assign(config, extended);

module.exports = config;
