/**
 * Created by alex on 10/3/17
 */

"use strict";

const Bb = require('bluebird');
const fs = Bb.promisifyAll(require('fs'));
const apiDoc = require('apidoc');
const config = require('config');
const apiDocConfig = {
  "title": `${config.appName} documentation`,
  "url":`http://${config.hostname}:6030/api/v1`,
  "sampleUrl": `http://${config.hostname}:6030/api/v1`,
};

// ApiDoc generation
function generate() {
  if (process.env.NODE_ENV === 'development') {
    fs.writeFileAsync('./apidoc.json', JSON.stringify(apiDocConfig))
    .then(() => {
      return apiDoc.createDoc({
        src: './app/',
        dest: './apidoc',
      });
    })
  }
}

module.exports = { generate };
