/**
 * Created by alex on 10/2/17
 */

"use strict";

const statuses = require('statuses');
const log = require('config/log')(module);

class HTTPError extends Error {
  constructor(token, message) {
    super(message);
    try {
      this.statusCode = statuses(token);
      this.message = message || statuses[this.statusCode];
    } catch (err) {
      log.error(`Unexpected http status token ${token}`, err.stack)
    }
  }
}

module.exports = HTTPError;