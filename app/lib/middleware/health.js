const config = require('config');

module.exports = (req, res) => {
  res.header("Content-Type",'application/json');
  res.send(JSON.stringify({
    appName: config.appName,
    upTime: process.uptime(),
    environment: process.env.NODE_ENV,
  }, null, 4));
}
