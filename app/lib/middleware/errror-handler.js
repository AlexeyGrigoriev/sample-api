/**
 * Created by alexey on 05.07.17.
 */


const config = require('config/index');

module.exports = (app) => {
  // development error handler
  // will print stacktrace
  if (config.isDev || config.isTest) {
    app.use((err, req, res, next) => {
      console.error(err.message, err.stack);
      res
        .status(err.status || err.statusCode || (err.httpStatus && err.httpStatus.code) || 500)
        .send({
          message: err.message,
          error: err,
        });
    });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use((err, req, res, next) => {
    res
      .status(err.status || (err.httpStatus && err.httpStatus.code) || 500)
      .send({
        message: err.message
      });
  });
};
