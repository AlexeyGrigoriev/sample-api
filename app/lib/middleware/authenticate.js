const config = require('config');
const jwt = require('jsonwebtoken');
const HTTPError = require('app/lib/errors/http.error');
const { User } = require('config/mongoose').models;

module.exports = function (req, res, next) {
  const authHeader = req.header('Authorization') || req.header('authorization')
  if (!authHeader) {
    return next(new HTTPError(401));
  }
  const token = authHeader.split(' ')[1];
  let decodedData;

  if (!token) return next(new HTTPError(401));

  try {
    decodedData = jwt.verify(token, config.secret);
  } catch (err) {
    return next(new HTTPError(401, err.message));
  }
  return User.findById(decodedData._id)
    .select('-__v -hashedPassword -salt')
    .lean()
    .exec()
    .then(user => {
      if (!user) {
        throw new HTTPError(401);
      }
      req.userData = user;

      next();
      return user;

    })
    .catch(next)
}