let { transport, mailOptions } = require('config/email');
const config = require('config');
const log = require('config/log')(module);
const weatherTpl = require('../../views/weather.template');
const emailTpl = require('../../views/email.template');

class EmailService {
  constructor() {
    if (config.isTest) {
      this.sentEmails = [];
    }
  }

  async send(email, message, weather) {
    const weatherHtml = weatherTpl(weather);
    const emailHtml = emailTpl({ message, weatherHtml });

    const options = Object.assign({}, mailOptions, {
      to: email,
      subject: `Message from ${config.appName}`,
      html: emailHtml,
    });

    try {
      const info = await transport.sendMailAsync(options);
      if (config.isTest) {
        this.sentEmails.push(info.message);
      }
      log.info('Message %s sent: %s', info.messageId, info.response || info.message);
      return { success: true };
    } catch (err) {
      log.error(err);
      return { success: false };
    }
  }
}

module.exports = new EmailService();
