const request = require('request');
const { apixu: { key } } = require('config');

module.exports = {
  getWeatherBy(location) {
    return new Promise((resolve, reject) => {
      request(
        `http://api.apixu.com/v1/current.json?key=${key}&q=${location}`,
        function (err, resp, body) {
          if (err) return reject(err);
          try {
            const result = JSON.parse(body);
            return resolve(result)
          } catch (parsingErr) {
            return reject(parsingErr);
          }
        }
      )
    })
  }
}