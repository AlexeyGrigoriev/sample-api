'use strict';

const test = require('express').Router();
const emailService = require('app/lib/services/email.service');
const apiEmuRouter = require('./api-emulator.routes');

test.use('/api-emulator', apiEmuRouter);

test.get('/emails', (req, res) => {
  return res.send(emailService.sentEmails);
});

test.post('/empty-emails', (req, res) => {
  emailService.sentEmails = [];
  return res.send(emailService.sentEmails);
});

module.exports = test;
