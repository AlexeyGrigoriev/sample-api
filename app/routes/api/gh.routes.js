/**
 * Created by alexey on 14.06.17.
 */

'use strict';

const router = require('express').Router();
const gitHubCtl = require('app/controllers/gh.controller');
/**
 * Not secured api
 */
router.post('/send-emails', gitHubCtl.sendEmails);

module.exports = router;