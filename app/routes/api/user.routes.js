'use strict';

const router = require('express').Router();
const userCtl = require('app/controllers/user.controller');
/**
 * sample rest api
 */
router.get('/:_id', userCtl.getOne);
router.get('/', userCtl.getList);
router.post('/', userCtl.createOne);
router.put('/:_id', userCtl.updateOne);
router.delete('/:_id', userCtl.deleteOne);

module.exports = router;