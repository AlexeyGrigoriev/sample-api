/**
 * Created by alexey on 14.06.17.
 */

'use strict';

const authCtl = require('app/controllers/auth.controller');
const HTTPError = require('../../../app/lib/errors/http.error');
const auth = require('express').Router();
const logger = require('morgan');

auth.use(logger('dev'));

auth.post('/signup', authCtl.signup);
auth.post('/signin', authCtl.signin);

/***
 * catch 404 and forward to error handler
 */
auth.use((req, res, next) => {
  next(new HTTPError(404));
});

module.exports = auth;
