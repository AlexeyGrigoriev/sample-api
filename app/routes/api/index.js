/**
 * Created by alexey on 14.06.17.
 */

'use strict';
const api = require('express').Router();
const logger = require('morgan');
const gitHubRouter = require('./gh.routes');
const userRouter = require('./user.routes');
const authRouter = require('./auth.routes');
const HTTPError = require('../../lib/errors/http.error');
const authenticate = require('app/lib/middleware/authenticate');

api.use(logger('dev'));

// Need auth
api.use([
  '/users',
  '/github',
], authenticate);

api.use('/auth', authRouter);
api.use('/users', userRouter);
api.use('/github', gitHubRouter);


/**
 * catch 404 and forward to error handler
 */
api.use((req, res, next) => {
  next(new HTTPError('404'));
});

module.exports = api;
