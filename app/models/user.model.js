/**
 * Created by alexey on 14.06.17.
 */

'use strict';

const modelName = 'User';
const crypto = require('crypto');

module.exports = function (mongoose) {
  const Schema = new mongoose.Schema({
    email: {
      type: String,
      unique: true,
      required: true,
    },
    avatarUrl: {
      type: String
    },
    token: String,
    hashedPassword: {
      type: String,
    },
    salt: {
      type: String,
    }
  }, {
    versionKey: false,
  });

  /**
   * Create instance method for hashing a password
   */
  Schema.methods.hashPassword = function (password) {
    if (this.salt && password) {
      return crypto.pbkdf2Sync(password, this.salt, 10000, 64, 'sha512').toString('base64');
    } else {
      return password;
    }
  };

  /**
   * Create instance method for authenticating user
   */
  Schema.methods.authenticate = function (password) {
    
    console.log(this.hashedPassword, this.hashPassword(password));
    return this.hashedPassword === this.hashPassword(password);
  };

  Schema.virtual('password')
  .set(function(password) {
    this._plainPassword = password;
    this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
    this.hashedPassword = this.hashPassword(password);
  })
  .get(function() {
    return this._plainPassword;
  });

  Schema.statics.findByEmail = function(email) {
    return this.findOne({ email })
  }
 
  mongoose.model(modelName, Schema);
};