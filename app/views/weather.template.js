module.exports = w => {
  if (!w) return '';
  return `
  <div class="pane">
    <h3>Location</h3>
  
    <p>Name: ${w.location.name}</p>
    <p>Region: ${w.location.region}</p>
    <p>Country: ${w.location.country}</p>
  
    <h3>Conditions</h3>

    <div>
      <p>${w.current.condition.text}</p>
      <img
        style="width: 36px;height: 36px;"
        src="http:${w.current.condition.icon}" 
        alt="icon ${w.current.condition.text}"
      >
    </div>
    <p>Temperature: ${w.current.temp_c} C</p>
    <p>Feels like: ${w.current.feelslike_c} C</p>

    <p>Pressure: ${w.current.pressure_mb} mm Hg</p>
    <p>Humidity: ${w.current.humidity} %</p>
  </div>
  `;
}