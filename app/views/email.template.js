module.exports = d => `<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <style>
    .pane {
      border: 1px solid white;
      padding: 20px;
      margin: 10px;
      background: #e8e0e0;
      box-shadow: 1px 1px 2px 2px grey;
    }
  </style>
</head>
<body>
  <div class="pane">
    <h3>Message</h3>
    <p>${d.message}</p>
    <p></p>
  </div>
  ${d.weatherHtml}
</body>
</html>`;