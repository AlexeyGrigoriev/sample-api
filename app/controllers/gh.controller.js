const BaseRestController = require('app/controllers/base-rest.controller');
const gh = require('config/github')
const name = 'GitHub';
const weatherService = require('../lib/services/weather.service')
const emailService = require('../lib/services/email.service')

/**
 * @apiDefine ResponseSuccess
 * @apiSuccess {String} username Github username
 * @apiSuccess {String} email public email from Github
 * @apiSuccess {Object} emailSent Status object of email sending
 * @apiSuccess {Boolean} emailSent.success Status of email sending
 * @apiSuccess {Boolean} withWeather Flag of weather attachment to email
 */

class GitHubController extends BaseRestController {

    /**
   * @api {post} /github/send-emails Send mails to GitHub users
   * @apiName Send emails
   * @apiGroup GitHub
   *
   * @apiParam {Array} usernames GitHub usernames list
   * @apiParam {String} message Massage to email
   *
   * @apiHeader {String} Authorization Users access JWT.
   *
   * @apiSampleRequest /github/send-emails
   *
   * @apiUse ResponseSuccess
   */
  async sendEmails({ body: { usernames, message } }) {
    return Promise.all(usernames.map(async username => {
      const user = gh.getUser(username)
      const { data: { email, location } } = await user.getProfile();
      
      if (!email) {
        return {
          username, 
          email: 'private', 
          emailSent: { success: false },
          withWeather: false,
        }
      }
      
      const weatherRes = location ? await weatherService.getWeatherBy(location) : null;
      const weather = weatherRes && !weatherRes.error ? weatherRes : null
      const result = await emailService.send(email, message, weather)
      
      return {
        username,
        email,
        emailSent: result,
        withWeather: !!weather
      };
    }));
  }

}


module.exports = new GitHubController(name);