const mg = require('config/mongoose');
const HTTPError = require('app/lib/errors/http.error');
const BaseController = require('app/controllers/base.controller');


class BaseRestController extends BaseController {
  constructor(name) {
    super();
    this.name = name;
    this.model = mg.models[name];
    this.populateFields = '';
  }

  getById(_id) {
    return this.model
    .findById(_id)
    .select('-__v')
    .populate(this.populateFields)
    .then(entity => {
      if (!entity) throw new HTTPError('NOT FOUND');
      return entity;
    });
  }

  createOne({ body, res }) {
    res.status(201);
    return this.model.create(body)
      .then(({_id}) => {
        return this.getById(_id);
      });
  }

  getOne({ params: { _id } }) {
    return this.getById(_id);
  }

  getList() {
    return this.model
      .find({})
      .populate(this.populateFields)
      .select('-__v');
  }

  updateOne({ params: { _id }, body }) {
    return this.getById(_id)
    .then(entity => {
      Object.assign(entity, body);
      return entity.save(entity);
    });
  }

  deleteOne({ params: { _id }, res }) {
    res.status(204);
    return this.getById(_id)
      .then(entity => entity.remove());
  }
}

module.exports = BaseRestController;
