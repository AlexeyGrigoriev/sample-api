/**
 * @apiDefine ResponseSuccess
 * @apiSuccess {Boolean} status Status of request.
 * @apiSuccess {Object} data Request result data
 */

const config = require('config');
const mg = require('config/mongoose');
const { User } = mg.models;
const log = require('config/log')(module);
const HTTPError = require('app/lib/errors/http.error');
const BaseController = require('app/controllers/base.controller');
const jwt = require('jsonwebtoken');
const ERR_MSG = require('config/messages/error-messages');

/**
 * Function for serialize and sigh user date with JWT
 * @param userData
 * @returns {{avatarUrl, token: *}}
 */
function sign({ _id, email, avatarUrl }) {
  const user = { _id, email, avatarUrl };
  const token = jwt.sign(user, config.secret);
  return { avatarUrl, token };
}

class AuthController extends BaseController {
  pre(ctx) {
    log.info('Will be called before any other method');
    return ctx;
  }
  /**
   * @api {post} /auth/signin Signin
   * @apiName Signin
   * @apiGroup Auth
   *
   * @apiSampleRequest /auth/signin
   *
   * @apiParam {String} email User's unique email.
   * @apiParam {String} password User's password.
   *
   * @apiSuccess {String} _id   User identifier.
   * @apiSuccess {String} email User email address
   * @apiSuccess {String} avatarUrl User's avatar url.
   * @apiSuccess {String} token User Access token (JWT).
   */
  async signin({ body: { email, password } }) {
    if (!email || !password) {
      throw new HTTPError(400, ERR_MSG.EMAIL_PASS_REQUIRED);
    }
    const user = await User.findByEmail(email)

    if (!user) throw new HTTPError(401, ERR_MSG.WRONG_LOGIN);
    if (!user.authenticate(password)) throw new HTTPError(401, ERR_MSG.WRONG_LOGIN)
    
    const { token } = sign(user);
    
    return { 
      email: user.email,
      avatarUrl: user.avatarUrl,
      token
    };
  }

  /**
   * @api {post} /auth/signup Signup
   * @apiName Signup
   * @apiGroup Auth
   *
   * @apiParam {String} email User email
   * @apiParam {String} password User password
   * @apiParam {File} avatar User's avatar image
   * 
   * @apiSuccess {String} avatarUrl User's avatar url.
   * @apiSuccess {String} token User Access token (JWT).
   *
   * @apiSampleRequest /auth/signup
   *
   * @apiUse ResponseSuccess
   */
  async signup({ body: { email, password }, files }) {
    let avatar;
    const newUserData = {
      email,
      password,
    }
    
    if (files && files.avatar) {
      avatar = files.avatar;
      await avatar.mv(`${__dirname}/../../public/${avatar.name}`)
      newUserData.avatarUrl = `http://${config.hostname}:${config.port}/${avatar.name}`
    }
    try {
      const userData = await User.create(newUserData);
      return sign(userData)
    } catch (err) {
      if (err.code === 11000) {
        throw new HTTPError(400, ERR_MSG.ALREADY_EXISTS)
      }
    }
  }
}

module.exports = new AuthController();