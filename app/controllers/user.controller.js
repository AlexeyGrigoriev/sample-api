const BaseRestController = require('app/controllers/base-rest.controller');
const name = 'User';


/**
 * @apiDefine RequestBody
 * @apiSuccess {Boolean} status Status of request.
 * @apiSuccess {Object} data Request result data
 */

 /**
 * @apiDefine ResponseSuccess
 * 
 * @apiSuccess {String} _id User identifier
 * @apiSuccess {String} email User email
 * @apiSuccess {String} avatarUrl Users avatar url
 */


/**
 * @apiDefine UserSchema
 * @apiParamExample {json} Request-Example:
 *     {
 *       "_id": "fqeqfqwf2qf24fq52",
 *       "email": "asdofijasio3fo3ijf",
 *       "avatarUrl": "http://hostname.com/avatar.png"
 *     }
 */


/**
 * @api {get} /users Get users list
 * @apiName Users list
 * @apiGroup User REST
 *
 *
 * @apiHeader {String} Authorization Users access JWT.
 *
 * @apiHeaderExample {json} Authorization example:
 *     {
 *       "Authorization": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiO
 *       iIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95Or
 *       M7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
 *     }
 *
 *
 * @apiSampleRequest /users
 *
 * @apiUse ResponseSuccess
 * @apiUse UserSchema
 */

/**
 * @api {post} /users Insert new user
 * @apiName Create user
 * @apiGroup User REST
 *
 * @apiSampleRequest /users
 *
 * @apiParam {String} email User email
 * @apiParam {String} password User password
 * @apiParam {String} avatarUrl User avatar url
 *
 * @apiUse ResponseSuccess
 */

/**
 * @api {get} /users/:_id Get user by id
 * @apiName User
 * @apiGroup User REST
 *
 * @apiParam {String} _id User identifier
 *
 * @apiSampleRequest /users/:_id
 *
 * @apiUse ResponseSuccess
 */

/**
 * @api {put} /users/:_id Update user
 * @apiName Update user
 * @apiGroup User REST
 *
 * @apiParam {String} _id User identifier
 *
 * @apiSampleRequest /users/:_id
 * @apiParam {String} email User email
 * @apiParam {String} avatarUrl User avatar url
 *
 * @apiUse ResponseSuccess
 */

/**
 * @api {delete} /users/:_id Delete user by id
 * @apiName Delete user
 * @apiGroup User REST
 *
 * @apiParam {String} _id User identifier
 *
 * @apiSampleRequest /users/:_id
 *
 * @apiUse ResponseSuccess
 */

class UserController extends BaseRestController {}

module.exports = new UserController(name);