const Bb = require('bluebird');
const HTTPError = require('app/lib/errors/http.error');
const ERR_MSG = require('config/messages/error-messages');

function checkAction (path) {
  const pathParts = path.split('/').filter(Boolean);
  if (pathParts.length === 0) return false;
  return !(/^\:.+/).test(pathParts.pop());
}

function hasId (path) {
  const pathParts = path.split('/').filter(Boolean);
  if (pathParts.length === 0) return false;
  for (let p of pathParts) {
    if ((/^\:.+/).test(p)) return true;
  }
  return false;
}

const interceptor = {
  get(target, property, receiver) {
    if (typeof target[property] !== 'function' ||
      target.nonInterceptable.includes(property)) {
      return target[property];
    }
    return function (req, res, next) {
      const { params, body, query, fields, files, userData } = req;
      const isAction = checkAction(req.route.path);
      const idExists = hasId(req.route.path);

      // handle `my` placeholder
      if (params._id === 'me') {
        if (!userData) {
          throw new HTTPError(403, ERR_MSG.MY_PLACEHOLDER_NOT_ALLOWED);
        }
        params._id = userData._id.toString();
      }

      const isUpdate = req.method !== 'GET'
      const isReplace = req.method === 'PUT' || req.method === 'PATCH';
      const isDelete = req.method === 'DELETE';
      const isInsert = req.method === 'POST' && !isAction;
      const isSelect = req.method === 'GET' && !isAction;
      const isSelectOne = isSelect && idExists;


      const ctx = {
        params, body, query, fields, files, userData,
        isAction, isUpdate, isDelete, isInsert,
        isSelect, isSelectOne, isReplace,
        req, res,
      };

      return Bb.try(() => target.pre(ctx))
      .then(target[property].bind(target))
      .then(target.post.bind(target))
      .then(target.responseHandler)
      .then(result => res.send(result))
      .catch(next);
    }
  }
};

class BaseController {
  constructor() {
    this.nonInterceptable = ['pre', 'post'];
    return new Proxy(this, interceptor);
  }
  // allows customize output structure
  // and keep same structure for all the responses
  responseHandler(result) {
    return result;
  }

  pre(ctx) {
    return ctx;
  }

  post(result) {
    return result;
  }
}

module.exports = BaseController;
