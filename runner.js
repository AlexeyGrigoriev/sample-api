/**
 * Created by alexey on 14.06.17.
 */

if (!process.env.NODE_ENV) {
  process.stdout.write('WARNING: Node environment is not defined\n');
  process.env.NODE_ENV = 'development';
}
process.stdout.write(`Starting in "${process.env.NODE_ENV}" environment\n`)

require('app-module-path').addPath(__dirname);


require(process.argv[2])(process.argv[3]);